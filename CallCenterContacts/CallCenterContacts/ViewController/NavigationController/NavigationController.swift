//
//  NavigationViewController.swift
//  CallCenterContacts
//
//  Created by jang gukjin on 2021/05/17.
//  Copyright © 2021 jang gukjin. All rights reserved.
//

import UIKit
import AVFoundation

class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if self.topViewController is UserDefaultViewController {
            
        } else {
            self.pushViewController(UserDefaultViewController(), animated: true)
            AudioServicesPlaySystemSound(1102)
        }
    }
}
